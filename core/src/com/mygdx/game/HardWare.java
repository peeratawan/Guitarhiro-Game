package com.mygdx.game;
import org.usb4java.Device;

public class HardWare {

	private McuWithPeriBoard peri;
	private Device[] devices;
	public boolean [] isSwitchPress;
	
	public HardWare() {
		isSwitchPress = new boolean [World.NBOFCOLOR];
		
		devices = McuBoard.findBoards();
    	
    	if (devices.length == 0) {
            System.out.format("** Practicum board not found **\n");
            return;
    	}
    	else {
            System.out.format("** Found %d practicum board(s) **\n", devices.length);
    	}
        peri = new McuWithPeriBoard(devices[0]);

        System.out.format("** Practicum board found **\n");
        System.out.format("** Manufacturer: %s\n", peri.getManufacturer());
        System.out.format("** Product: %s\n", peri.getProduct());

	}
	
	public void update() {
		for(int i=0;i<World.NBOFCOLOR;i++) {
			isSwitchPress[i] = peri.getSwitch(i+1);
		}
		
	}
}
