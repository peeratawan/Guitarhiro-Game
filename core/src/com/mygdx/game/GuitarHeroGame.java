package com.mygdx.game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class GuitarHeroGame extends Game {
	public static final int HEIGHT = 900;
	public static final int WIDTH = 1800;
	public SpriteBatch batch;
	/*private McuWithPeriBoard[] controllers;
	
	public GuitarHeroGame() {//////////////////////////////
		System.out.println("** Initializing USB");
        McuBoard.initUsb();
        //launch(args);
        System.out.println("** Cleaning up USB");
        McuBoard.cleanupUsb();
	}////////////////////////////////
	
	//////////////////////////////////
	private void initHardware() {
        Device[] devices = McuBoard.findBoards();
        System.out.format("** Found %d practicum board(s)\n", devices.length);
        controllers = new McuWithPeriBoard[devices.length];
        for (int i=0; i<devices.length; i++) {
            controllers[i] = new McuWithPeriBoard(devices[i]);
            System.out.format("-- Board #%d: Manufacturer: %s Product: %s\n",
                    i, controllers[i].getManufacturer(), controllers[i].getProduct());
        }        
    }
    public void init() {
        initHardware();
        if (controllers.length == 0) {
            System.out.println("** Please attach a practicum board");
            System.out.println("** Cleaning up USB");
            McuBoard.cleanupUsb();
            Platform.exit();
            System.exit(0);
        }
    }
	//////////////////////////////////
	*/
	@Override
	public void create () {
		batch = new SpriteBatch();
		setScreen(new GameScreen(this));
	}

	public void render (float delta) {
		System.out.println("hello " + delta);
	}
	
	@Override
	public void dispose () {
		batch.dispose();
	}
}
